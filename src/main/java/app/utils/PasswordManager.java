package app.utils;

import java.util.Arrays;
import java.util.Objects;

public class PasswordManager {
    static String delimiter = "100";
    
    public PasswordManager() {}
    
    public String getHash(String openPassword) {

        if (openPassword.equals(""))
            return openPassword;

        StringBuilder magicString = new StringBuilder("12345678");
        String tempOpenPassword = openPassword;
        StringBuilder hashedPassword = new StringBuilder();

        if (magicString.length() < tempOpenPassword.length()) {
            int dif = tempOpenPassword.length() - magicString.length();
            tempOpenPassword = tempOpenPassword.substring(0, tempOpenPassword.length() - dif);
        }

        String[] t1 = new String[tempOpenPassword.length()];
        String[] t2 = new String[tempOpenPassword.length()];

        for (int i = 0; i < tempOpenPassword.length(); i++) {
            t1[i] = String.valueOf(tempOpenPassword.charAt(i));
            t2[i] = String.valueOf(tempOpenPassword.charAt(i));
        }

        int[] key = new int[t1.length];
        Arrays.sort(t2);

        for (int i = 0; i < t1.length; i++) {
            for (int j = 0; j < t2.length; j++) {
                if (Objects.equals(t1[i], t2[j])) {
                    key[i] = j;
                    t2[j] = delimiter;
                    break;
                }
            }
        }

        if (magicString.length() % key.length != 0) {
            for (int i = 0; i < magicString.length() % key.length; i++)
                magicString.append(' ');
        }

        for (int i = 0; i < magicString.length(); i += key.length) {
            StringBuilder str = new StringBuilder();

            for (int j = 0; j < key.length; j++)
                str.append(magicString.charAt(i + j));

            for (int k : key)
                hashedPassword.append(str.charAt(k));
        }
        return hashedPassword.toString();
    }
}
