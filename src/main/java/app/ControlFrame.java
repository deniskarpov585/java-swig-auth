package app;

import app.models.Admin;
import app.models.BaseUser;
import app.models.User;
import net.miginfocom.swing.MigLayout;
import app.repo.UserRepo;
import javax.swing.*;
import javax.swing.ButtonGroup;
import java.awt.*;
import java.util.Map;

public class ControlFrame extends JFrame {

    private BaseUser baseUser;
    private UserRepo userRepo;
    private boolean isAdmin;
    private boolean isFirstEnter;

    JRadioButton changePasswordRb = new JRadioButton();
    JRadioButton handleUsersRb = new JRadioButton();
    JRadioButton addUsersRb = new JRadioButton();
    JLabel changeEmptyPassportLabel = new JLabel();

    Map<String, JRadioButton> radioButtons = Map.of(
            "change", changePasswordRb,
            "list", handleUsersRb,
            "add", addUsersRb
    );

    ButtonGroup group = new ButtonGroup();

    ControlFrame(BaseUser baseUser, boolean isAdmin, boolean isFirstEnter, UserRepo userRepo) {
        JPanel controlPanel = new JPanel(new MigLayout());
        this.setSize(320, 260);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setTitle("Control Panel");

        this.isAdmin = isAdmin;
        this.userRepo = userRepo;
        this.isFirstEnter = isFirstEnter;

        if (isAdmin)
            this.baseUser = (Admin) baseUser;
        else {
            this.baseUser = (User) baseUser;
        }

        JPanel radioPanel = createButtonsPanel();
        JPanel buttonsPanel = createLoginButtonsPanel();

        controlPanel.add(radioPanel, "wrap, gapleft 20");
        controlPanel.add(buttonsPanel);

        this.add(controlPanel);
        this.setVisible(true);
        this.setResizable(true);
    }

    JPanel createButtonsPanel() {
        JPanel panel = new JPanel(new MigLayout("wrap, insets 5"));

        changePasswordRb.setText("Change password");
        handleUsersRb.setText("Users list");
        addUsersRb.setText("Add users");

        group.add(changePasswordRb);
        group.add(handleUsersRb);
        group.add(addUsersRb);

        if (isAdmin) {
            if (!isFirstEnter) {
                handleUsersRb.setEnabled(true);
                addUsersRb.setEnabled(true);
                changeEmptyPassportLabel.setEnabled(false);
                changeEmptyPassportLabel.setText("");
            } else {
                handleUsersRb.setEnabled(false);
                addUsersRb.setEnabled(false);
                changeEmptyPassportLabel.setEnabled(true);
                changeEmptyPassportLabel.setText(Messages.adminChangeEmptyPassport);
                changeEmptyPassportLabel.setForeground(new Color(206, 34, 34));
            }
        } else {
            handleUsersRb.setEnabled(false);
            addUsersRb.setEnabled(false);
        }

        panel.add(changePasswordRb);
        panel.add(handleUsersRb);
        panel.add(addUsersRb);
        panel.add(changeEmptyPassportLabel);

        return panel;
    }

    JPanel createLoginButtonsPanel() {
        JPanel panel = new JPanel(new MigLayout("wrap 2, insets 30"));

        JButton okButton = new JButton("Ok");
        okButton.addActionListener(e -> {
            radioButtons.forEach((id, rb) -> {
                if (rb.isSelected()) {
                    if (id == "change") {
                        ChangePasswordFrame f = new ChangePasswordFrame(baseUser, userRepo);
                        this.dispose();
                    } else if (id == "list") {
                        ListUsersFrame f = new ListUsersFrame((Admin) baseUser, userRepo);
                    } else if (id == "add") {
                        AddUsersFrame f = new AddUsersFrame((Admin) baseUser, userRepo);
                    }
                }
            });
        });
        JButton closeButton = new JButton("Close");
        closeButton.addActionListener(e -> {
            this.dispose();
        });

        panel.add(okButton);
        panel.add(closeButton, "wrap");

        return panel;
    }


}
