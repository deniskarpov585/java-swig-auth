package app.models;

import java.io.Serializable;

public class BaseUser implements Serializable {
    private static final long serialVersionUID = 0L;
    private String login;
    private String password;

    public BaseUser(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public BaseUser() {}

    public String getLogin() { return this.login; }
    public void setLogin(String newLogin) { this.login = newLogin; }
    public String getPassword() { return this.password; }
    public void setPassword(String newPassword) { this.password = newPassword; }
}
