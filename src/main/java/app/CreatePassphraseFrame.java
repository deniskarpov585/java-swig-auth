package app;

import lombok.SneakyThrows;
import net.miginfocom.swing.MigLayout;
import app.repo.Database;
import app.repo.UserRepo;
import app.utils.CryptoController;

import javax.swing.*;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

public class CreatePassphraseFrame extends JFrame {

    private CryptoController cryptoController = new CryptoController();
    private JPasswordField passphraseField;
    private JPasswordField confirmField;

    private static Logger LOGGER = Logger.getLogger(UserRepo.class.getName());

    CreatePassphraseFrame() {
        JPanel passPhrasePanel = new JPanel();
        this.setSize(370, 180);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Create Passphrase");

        passPhrasePanel.add(createEnterPassPhrasePanel());
        passPhrasePanel.add(createButtonsPanel());
        this.add(passPhrasePanel);
        this.setVisible(true);
        this.setResizable(true);
    }

    private JPanel createButtonsPanel() {
        JPanel panel = new JPanel(new MigLayout("wrap 2", "", ""));
        JButton okButton = new JButton("Ok");
        okButton.addActionListener(e -> {
            if (!passphraseField.getText().equals(confirmField.getText())) {
                JOptionPane.showMessageDialog(
                        this,
                        Messages.mismatchPassphrase,
                        "Exit",
                        JOptionPane.ERROR_MESSAGE
                );
            } else {
                try {
                    cryptoController.generateKey(passphraseField.getText());
                    Database.createDatabase(Database.adminDatabase);
                } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
                    LOGGER.info("");
                }
                new LoginFrame(cryptoController);
            }
            this.dispose();
        });
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> {
            JOptionPane.showMessageDialog(
                    this,
                    Messages.refuseToEnterPassPhrase,
                    "Exit",
                    JOptionPane.INFORMATION_MESSAGE
            );
            this.dispose();
        });
        panel.add(okButton);
        panel.add(cancelButton, "gapleft 24");
        return panel;
    }

    private JPanel createEnterPassPhrasePanel() {
        JPanel panel = new JPanel(new MigLayout("wrap 2, insets 10"));
        JLabel passPhraseLabel = new JLabel("Passphrase: ");
        passphraseField = new JPasswordField(14);
        JLabel confirmLabel = new JLabel("Confirm: ");
        confirmField = new JPasswordField(14);

        panel.add(passPhraseLabel);
        panel.add(passphraseField);
        panel.add(confirmLabel);
        panel.add(confirmField);
        return panel;
    }
}
