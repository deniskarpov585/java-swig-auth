package app.repo;

import kotlin.Pair;
import app.models.User;
import app.utils.PasswordManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class UserRepo implements RepoInterface {

    private static Logger LOGGER = Logger.getLogger(UserRepo.class.getName());
    private final List<User> usersList = new ArrayList<>();
    private final PasswordManager passwordManager = new PasswordManager();

    public void addUser(User user) {
        usersList.add(user);
        Database.saveUsersData(usersList, Database.usersDatabase);
    }

    public User reloadUserData(User currentUser) {
        User oldUserData = getUser(currentUser.getLogin());

        User foundUser = Objects.requireNonNull(Database.readUsers(Database.usersDatabase))
                .stream()
                .filter(usr -> Objects.equals(usr.getLogin(), oldUserData.getLogin()))
                .collect(Collectors.toList()).get(0);
        return foundUser;
    }

    public void changeUserData(User user) {
        int index = 0;
        for (int i = 0; i < usersList.size(); i++) {
            if (Objects.equals(usersList.get(i).getLogin(), user.getLogin()) &&
                    Objects.equals(usersList.get(i).getPassword(), user.getPassword())) {
                index = i;
                break;
            }
        }
        usersList.set(index, user);
    }

    public List<User> getUsersList() {
        return usersList;
    }

    private User getUser(String login) {
        return (User) usersList.stream().filter(u -> Objects.equals(u.getLogin(), login))
                .collect(Collectors.toList()).get(0);
    }

    private void fillUsersList() {
        usersList.addAll(Objects.requireNonNull(Database.readUsers(Database.usersDatabase)));
    }

    public boolean isUserExists(String login) {
        fillUsersList();
        return usersList.stream().filter(user -> Objects.equals(user.getLogin(), login)).count() == 1;
    }

    public Pair<Boolean, StatusCode> auth(String login, String openPassword) {
        User user = getUser(login);
        String hashedPassword = passwordManager.getHash(openPassword);

        if (user.isAccountBlocked()) {
            LOGGER.info(String.format("User account %s is blocked", user.getLogin()));
            return  new Pair<Boolean, StatusCode>(false, StatusCode.CODE_BLOCKED);
        }

        ArrayList<User> allUsers = Database.readUsers(Database.usersDatabase);
        if (allUsers != null) {
            boolean correct = false;
            for (User u : allUsers) {
                if (checkLoginData(u, login, hashedPassword)) {
                    correct = true;
                    break;
                }
            }
            if (correct) {
                LOGGER.info("The user has logged in\n");
                return new Pair<Boolean, StatusCode>(true, StatusCode.CODE_CORRECT);
            } else {
                LOGGER.info("The user has not logged in. Invalid password");
                return new Pair<Boolean, StatusCode>(false, StatusCode.CODE_NOT_AUTH);
            }
        } else {
            return new Pair<Boolean, StatusCode>(false, StatusCode.CODE_NOT_AUTH);
        }
    }

}
