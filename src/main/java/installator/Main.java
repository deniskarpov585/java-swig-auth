package installator;

import app.Messages;
import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinReg;
import net.miginfocom.swing.MigLayout;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.logging.Logger;

public class Main extends JFrame {

    JButton chooseDirButton;
    JTextField chooseDirTextField;
    JLabel registryKeyNameLabel;
    JTextField registryKeyNameTextField;
    JLabel programNameLabel;
    JTextField programNameTextField;
    JButton installButton;
    JButton closeButton;

    private static Logger LOGGER = Logger.getLogger(Main.class.getName());


    Main() {
        JPanel mainPanel = new JPanel();
        mainPanel.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));

        mainPanel.add(createDirPanel());
        mainPanel.add(namePanel());
        mainPanel.add(controlPanel());

        this.setSize(520, 250);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Installator");
        this.setResizable(true);
        this.add(mainPanel);
        this.setVisible(true);
    }

    private JPanel createDirPanel() {
        JPanel panel = new JPanel(new MigLayout("wrap 2", "", ""));
        chooseDirTextField = new JTextField(30);
        chooseDirTextField.setEnabled(false);

        chooseDirButton = new JButton("Choose directory");
        chooseDirButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            fileChooser.setDialogTitle("Choose directory");

            int option = fileChooser.showOpenDialog(this);
            if (option == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                chooseDirTextField.setText(file.getAbsolutePath());
            }
            installButton.setEnabled(!chooseDirTextField.getText().equals(""));
        });
        panel.add(chooseDirButton);
        panel.add(chooseDirTextField, "span");
        return panel;
    }

    private JPanel namePanel() {
        JPanel panel = new JPanel(new MigLayout("wrap 2", "", ""));
        registryKeyNameLabel = new JLabel("Registry key name");
        registryKeyNameTextField = new JTextField(15);
        registryKeyNameTextField.setText("Golovin");
        programNameLabel = new JLabel("Program name");
        programNameTextField = new JTextField(15);
        programNameTextField.setText("Lab6");

        panel.add(registryKeyNameLabel);
        panel.add(registryKeyNameTextField);
        panel.add(programNameLabel);
        panel.add(programNameTextField);
        return panel;
    }

    private JPanel controlPanel() {
        JPanel panel = new JPanel(new MigLayout("wrap 2", "", ""));
        installButton = new JButton("Install");
        installButton.setEnabled(!chooseDirTextField.getText().equals(""));
        installButton.addActionListener(this::handleInstallButton);
        closeButton = new JButton("Close");
        closeButton.addActionListener(e -> dispose());
        panel.add(installButton);
        panel.add(closeButton);
        return panel;
    }

    private void handleInstallButton(ActionEvent actionEvent) {
        String dirPath = chooseDirTextField.getText();
        File destDir = new File(dirPath);
        if (!destDir.exists())
            destDir.mkdir();
        boolean ok = true;
        try {
            var programName = programNameTextField.getText();
            if (!Files.exists(Paths.get(dirPath + "\\" + programName + ".jar"))) {
                FileUtils.copyDirectory(new File(Paths.get(".").toAbsolutePath().normalize().toString() + "\\out\\artifacts\\app_jar\\"), destDir);
                writeWindowsRegistry();

                boolean successRenaming = new File(dirPath + "\\app.jar").renameTo(new File(dirPath + "\\" + programName + ".jar"));
                if (!successRenaming)
                    throw new Exception();

                File batFileName = new File(dirPath + "\\" + programName + "Bat.bat");
                FileOutputStream fos = new FileOutputStream(batFileName);
                DataOutputStream dos = new DataOutputStream(fos);

                dos.writeBytes("java -cp " + programName + ".jar app.Main");
                dos.writeBytes("\n");
                dos.close();
                ok = true;
            }
        } catch (Exception ex) {
            ok = false;
            LOGGER.warning("Installator exception: " + ex.getMessage());
        }
        JOptionPane.showMessageDialog(
                this,
                (ok) ? Messages.successInstallation : Messages.failInstallation,
                (ok) ? "Success" : "Fail",
                (ok) ? JOptionPane.INFORMATION_MESSAGE : JOptionPane.ERROR_MESSAGE
        );
    }


    private void writeWindowsRegistry() throws NoSuchAlgorithmException {
        byte[] hash = new SystemInfo().generateHashCode();
        Advapi32Util.registryCreateKey(WinReg.HKEY_CURRENT_USER, "Software\\\\" + registryKeyNameTextField.getText());
        Advapi32Util.registrySetStringValue(WinReg.HKEY_CURRENT_USER, "Software\\\\" + registryKeyNameTextField.getText(),
                "Signature",
                Arrays.toString(hash)
        );
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        new Main();
    }
}