package installator;

import lombok.AllArgsConstructor;

import java.awt.*;
import java.io.File;
import java.lang.management.ManagementFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.sun.management.OperatingSystemMXBean;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class SystemInfo {
    private String username;
    private String computerName;
    private String pathToOSWindowsDir;
    private String pathToSystemFilesOSWindowsDir;
    private int mouseButtonsCount;
    private int windowsWidth;
    private long ramVolume;
    private long diskVolume;

    public SystemInfo toSystemInfo() {
        return new SystemInfo(
                System.getProperty("user.name"),
                System.getenv("COMPUTERNAME"),
                System.getenv("windir"),
                System.getenv("SystemRoot"),
                MouseInfo.getNumberOfButtons(),
                Toolkit.getDefaultToolkit().getScreenSize().width,
                ((OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean()).getTotalPhysicalMemorySize(),
                new File("/").getTotalSpace()
        );
    }



    public byte[] generateHashCode() throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(toSystemInfo().toString().getBytes());
        return md.digest();
    }

    @Override
    public String toString() {
        return "SystemInfo{" +
                "username='" + username + '\'' +
                ", computerName='" + computerName + '\'' +
                ", pathToOSWindowsDir='" + pathToOSWindowsDir + '\'' +
                ", pathToSystemFilesOSWindowsDir='" + pathToSystemFilesOSWindowsDir + '\'' +
                ", mouseButtonsCount=" + mouseButtonsCount +
                ", windowsWidth=" + windowsWidth +
                ", ramVolume=" + ramVolume +
                ", diskVolume=" + diskVolume +
                '}';
    }
}
