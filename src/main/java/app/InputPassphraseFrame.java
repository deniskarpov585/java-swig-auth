package app;

import net.miginfocom.swing.MigLayout;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;

import app.repo.AdminRepo;
import app.repo.Database;
import app.utils.CryptoController;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

public class InputPassphraseFrame extends JFrame {

    private CryptoController cryptoController = new CryptoController();
    private AdminRepo adminRepo = new AdminRepo();
    private JPasswordField passphraseField;

    private static Logger LOGGER = Logger.getLogger(Database.class.getName());

    InputPassphraseFrame() {
        JPanel passPhrasePanel = new JPanel();
        this.setSize(330, 240);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Input Passphrase");

        passPhrasePanel.add(createEnterPassPhrasePanel());
        passPhrasePanel.add(createButtonsPanel());
        this.add(passPhrasePanel);
        this.setVisible(true);
        this.setResizable(false);
    }

    private JPanel createButtonsPanel() {
        JPanel panel = new JPanel(new MigLayout("wrap 2", "", ""));
        JButton okButton = new JButton("Ok");
        okButton.addActionListener(e -> {
            try {
                cryptoController.calculateKey(passphraseField.getText());
                cryptoController.decryptFile();
                new LoginFrame(cryptoController);
            } catch (IOException | NoSuchAlgorithmException |
                    NoSuchPaddingException | InvalidKeyException |
                    BadPaddingException | IllegalBlockSizeException |
                    InvalidAlgorithmParameterException ioException) {
                LOGGER.info("Exception while decrypting: " + ioException + "\n");
                JOptionPane.showMessageDialog(
                        this,
                        Messages.invalidPassphrase,
                        "Exit",
                        JOptionPane.ERROR_MESSAGE
                );
            }
            this.dispose();

        });
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> {
            JOptionPane.showMessageDialog(
                    this,
                    Messages.refuseToEnterPassPhrase,
                    "Exit",
                    JOptionPane.INFORMATION_MESSAGE
            );
            this.dispose();
        });
        panel.add(okButton);
        panel.add(cancelButton);
        return panel;
    }

    private JPanel createEnterPassPhrasePanel() {
        JPanel panel = new JPanel(new MigLayout("wrap, insets 10"));
        JLabel passphraseLabel = new JLabel("Input decrypt passphrase");
        passphraseField = new JPasswordField(17 );

        panel.add(passphraseLabel);
        panel.add(passphraseField);
        return panel;
    }
}
