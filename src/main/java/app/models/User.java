package app.models;

import java.io.Serializable;

public class User extends BaseUser implements Serializable {

    private static final long serialVersionUID = 2L;

    public User() {
        super();
    }

    public User(String login, String password) {
        super(login, password);
    }

    public boolean validatePassword(String password) {
        boolean littleFound = false;
        boolean bigFound = false;

        for (int i = 0; i < password.length(); i++) {
            char ch = password.charAt(i);
            if (Character.isLowerCase(ch)) {
                littleFound = true;
            } else if (Character.isUpperCase(ch)) {
                bigFound = true;
            }
            if (littleFound && bigFound) return true;
        }
        return false;
    }

    private transient boolean isAccountBlocked = false;
    private transient boolean isRestricted = false;

    public boolean isAccountBlocked() { return isAccountBlocked; }
    public void setAccountBlocked(boolean accountBlocked) { isAccountBlocked = accountBlocked; }
    public boolean isRestricted() { return isRestricted; }
    public void setRestricted(boolean restricted) { isRestricted = restricted; }
}
