package app.utils;

import lombok.SneakyThrows;
import app.repo.Database;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.util.logging.Logger;

public class CryptoController {

    private byte[] key;
    private byte[] salt;
    private String saltFile = "salt.txt";
    private String encryptedAdminFile = "encrypted.admin.json";
    private String encryptedUsersFile = "encrypted.users.json";
    public  String tmpUsersFile = "tmp.user.json";
    public  String tmpAdminFile = "tmp.admin.json";
    private static String cipherAlgo = "AES/OFB/PKCS5Padding";

    private static Logger LOGGER = Logger.getLogger(Database.class.getName());

    public CryptoController() {}

    public boolean isPassphraseExists() {
        return new File(encryptedAdminFile).exists();
    }

    public void generateKey(String passphrase) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD2");
        this.salt = generateSalt();

        try {
            saveSalt(salt);
        } catch (IOException ex) {
            LOGGER.warning("Error while saving salt: " + ex);
        }
        md.update(salt);
        byte[] passphraseBytes = passphrase.getBytes();
        this.key = md.digest(passphraseBytes);
    }

    private byte[] generateSalt() {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[16];
        random.nextBytes(salt);
        return salt;
    }

    public void calculateKey(String passphrase) throws IOException, NoSuchAlgorithmException {
        this.salt = readSalt();
        MessageDigest md = MessageDigest.getInstance("MD2");
        md.update(salt);
        byte[] passphraseBytes = passphrase.getBytes();
        this.key = md.digest(passphraseBytes);
    }

    private void saveSalt(byte[] salt) throws IOException {
        FileOutputStream outputStream = new FileOutputStream(saltFile);
        outputStream.write(salt);
        outputStream.close();
    }

    private byte[] readSalt() throws IOException {
        FileInputStream inputStream = new FileInputStream(saltFile);
        int content;
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();
        while ((content = inputStream.read()) != -1)
            buffer.write((char) content);
        this.salt = buffer.toByteArray();
        inputStream.close();
        return this.salt;
    }

    // расшифровка файла
    public void decryptFile()
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException,
            IOException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException {
        var cipher = Cipher.getInstance(cipherAlgo);
        var key = new SecretKeySpec(this.key, "AES");
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(new byte[16]));
        decrypt(encryptedAdminFile, cipher);
        decrypt(encryptedUsersFile, cipher);
        LOGGER.info("Files successfully decrypted!");
    }

    private void decrypt(String fileName, Cipher cipher)
            throws IOException, IllegalBlockSizeException, BadPaddingException {
        var encryptedFileInput = new File(fileName);
        var inputStream = new FileInputStream(encryptedFileInput);
        var inputBytes = new byte[(int) encryptedFileInput.length()];
        inputStream.read(inputBytes);

        byte[] outputBytes = cipher.doFinal(inputBytes);

        String tmpFile = (fileName.equals(encryptedAdminFile)) ? tmpAdminFile : tmpUsersFile;
        var decryptedFileOutput = new File(tmpFile);
        var outputStream = new FileOutputStream(decryptedFileOutput);
        outputStream.write(outputBytes);

        inputStream.close();
        outputStream.close();
    }

    // сохраняем зашифрованный файл
    public void saveEncryptedFile()
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException,
            IOException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException {
        var cipher = Cipher.getInstance(cipherAlgo);
        var key = new SecretKeySpec(this.key, "AES");
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(new byte[16]));

        save(tmpAdminFile, cipher);
        save(tmpUsersFile, cipher);
    }

    private void save(String tmpFile, Cipher cipher)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException,
            IOException, BadPaddingException, IllegalBlockSizeException {

        var tmpFileInput = new File(tmpFile);
        var inputStream = new FileInputStream(tmpFileInput);
        var inputBytes = new byte[(int) tmpFileInput.length()];
        inputStream.read(inputBytes);

        var outputBytes = cipher.doFinal(inputBytes);

        String encryptedFile = (tmpFile.equals(tmpAdminFile)) ? encryptedAdminFile : encryptedUsersFile;
        var encryptedFileOutput = new File(encryptedFile);
        var outputStream = new FileOutputStream(encryptedFileOutput);
        outputStream.write(outputBytes);

        inputStream.close();
        outputStream.close();
    }

    public void deleteTmpFiles() {
        try {
            Files.delete(Paths.get(tmpAdminFile));
            Files.delete(Paths.get(tmpUsersFile));
            LOGGER.info("Tmp files successful deleted!");
        } catch (IOException e) {
            LOGGER.warning(String.format("Deleting tmp file error: %s", e.getMessage()));
        }
    }



}