package app;

import app.utils.CryptoController;
import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinReg;
import installator.SystemInfo;
import lombok.Getter;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.util.Arrays;
import java.util.logging.Logger;

public class RegistryNameFrame extends JFrame {

    JTextField registryKeyNameTextField;

    private static Logger LOGGER = Logger.getLogger(RegistryNameFrame.class.getName());

    @Getter private String regInfo = "";
    RegistryNameFrame() {
        JPanel mainPanel = new JPanel(new MigLayout("wrap 2", "", "[][]20[]"));
        mainPanel.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));

        JLabel registryKeyNameLabel = new JLabel("Registry key name");
        registryKeyNameTextField = new JTextField(30);
        registryKeyNameTextField.setText("Golovin");

        mainPanel.add(registryKeyNameLabel, "wrap");
        mainPanel.add(registryKeyNameTextField, "wrap");
        mainPanel.add(createControlPanel());

        this.setSize(380, 200);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setTitle("Installator");
        this.add(mainPanel);
        this.setResizable(true);
        this.setVisible(true);
    }

    private JPanel createControlPanel() {
        JPanel panel = new JPanel(new MigLayout("wrap 2"));
        JButton okButton = new JButton("Ok");
        okButton.addActionListener(e -> {
            try {
                byte[] currentHash = new SystemInfo().generateHashCode();
                var regData = Advapi32Util.registryGetStringValue(
                        WinReg.HKEY_CURRENT_USER,
                        "Software\\\\" + registryKeyNameTextField.getText().trim(),
                        "Signature");
                if (!(Arrays.toString(currentHash)).equals(regData)) {
                    throw new Exception("hash not equals");
                }
                else {
                    CryptoController cryptoController = new CryptoController();
                    if (cryptoController.isPassphraseExists())
                        new InputPassphraseFrame();
                    else {
                        new CreatePassphraseFrame();
                    }
                }
                this.dispose();
            } catch (Exception ex) {
                LOGGER.warning("Exception while reading register data: " + ex);
                JOptionPane.showMessageDialog(
                        this,
                        Messages.signatureMismatch,
                        "Error",
                        JOptionPane.ERROR_MESSAGE
                );
            }
        });
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> this.dispose());
        panel.add(okButton);
        panel.add(cancelButton);
        return panel;
    }

}
