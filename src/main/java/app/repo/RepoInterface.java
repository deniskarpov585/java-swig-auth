package app.repo;

import app.models.BaseUser;
import java.util.Objects;

public interface RepoInterface {
    default boolean checkLoginData(BaseUser user, String login, String hashedPassword) {
        return Objects.equals(user.getPassword(), hashedPassword) && Objects.equals(user.getLogin(), login);
    }
}
