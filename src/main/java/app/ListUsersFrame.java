package app;

import app.models.Admin;

import app.models.User;
import net.miginfocom.swing.MigLayout;
import app.repo.Database;
import app.repo.UserRepo;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.List;

public class ListUsersFrame extends JFrame {

    private UserRepo userRepo;
    private List<User> userList;
    private User currentUser;
    private int userNumber = 0;

    JTextField firstNameTextField = new JTextField(15);
    private JCheckBox blockCb = new JCheckBox("Blocking");
    private JCheckBox constraintsCb = new JCheckBox("Constraints");
    private JLabel errorLabel = new JLabel();

    ListUsersFrame(Admin admin, UserRepo userRepo) {
        JPanel listPanel = new JPanel();
        listPanel.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));

        this.setSize(450, 250);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setTitle("Users list");

        this.userRepo = userRepo;
        userList = userRepo.getUsersList();
        currentUser = (userList.size() != 0) ? userList.get(userNumber) : null;

        JPanel userInfoPanel = createUserInfo();
        JPanel optionsPanel = createOptions();
        JPanel navigateAndExitButtonsPanel = createNavigateAndExitButtonsPanel();

        listPanel.add(userInfoPanel);
        listPanel.add(optionsPanel);
        listPanel.add(navigateAndExitButtonsPanel);
        this.add(listPanel);
        this.setResizable(true);
        this.setVisible(true);
    }

    JPanel createOptions() {
        JPanel panel = new JPanel(new MigLayout());
        panel.add(blockCb, "cell 1 0");
        panel.add(constraintsCb, "cell 1 1");
        return panel;
    }

    void setupButtonSize(JButton button) {
        button.setPreferredSize(new Dimension(120, 10));
    }

    JPanel createNavigateAndExitButtonsPanel() {
        JPanel panel = new JPanel(new MigLayout("wrap 2", "[]20[]"));
        panel.setBorder(new EmptyBorder(20, 10, 10, 10));
        JButton nextButton = new JButton("Next");
        setupButtonSize(nextButton);
        JButton saveButton = new JButton("Save");
        setupButtonSize(saveButton);
        JButton okButton = new JButton("Ok");
        setupButtonSize(okButton);
        JButton refreshButton = new JButton("Refresh");
        setupButtonSize(refreshButton);

        nextButton.addActionListener(e -> {
            nextUser();
        });

        saveButton.addActionListener(e -> {
            if (currentUser != null)
                handleSaveButton();
        });
        okButton.addActionListener(e -> {
            this.dispose();
        });

        refreshButton.addActionListener(e -> {
            firstNameTextField.setText("");
            blockCb.setSelected(false);
            constraintsCb.setSelected(false);
        });

        panel.add(nextButton);
        panel.add(saveButton);
        panel.add(okButton);
        panel.add(refreshButton);

        return panel;
    }

    void nextUser() {
        if (userNumber >= userList.size()) {
            userNumber = 0;
        } else {
           userNumber++;
        }

        if (userNumber == userList.size()) userNumber = 0;

        currentUser = (userList.size() != 0) ? userList.get(userNumber) : null;
        if (currentUser != null)
            updateFrame();
    }

    void updateFrame() {
        firstNameTextField.setText(currentUser.getLogin());
        blockCb.setSelected(currentUser.isAccountBlocked());
        constraintsCb.setSelected(currentUser.isRestricted());
    }

    JPanel createUserInfo() {
        JPanel panel = new JPanel();
        panel.setLayout(new MigLayout("wrap 2, insets 5"));

        JLabel firstNameLabel = new JLabel("Username");

        checkEmptyUserList();
        firstNameTextField.setText((userList.size() != 0) ? userList.get(userNumber).getLogin() : "");
        blockCb.setSelected(currentUser != null && currentUser.isAccountBlocked());
        constraintsCb.setSelected(currentUser != null && currentUser.isRestricted());

        panel.add(firstNameLabel);
        panel.add(firstNameTextField);
        panel.add(errorLabel, "span 2");
        return panel;
    }

    void checkEmptyUserList() {
        if (currentUser == null) {
            errorLabel.setVisible(true);
            errorLabel.setText(Messages.emptyUserList);
            errorLabel.setForeground(new Color(206, 34, 34));
        } else {
            errorLabel.setText("");
            errorLabel.setVisible(false);
        }
    }

    void handleSaveButton() {
        currentUser.setAccountBlocked(blockCb.isSelected());
        currentUser.setRestricted(constraintsCb.isSelected());

        userRepo.changeUserData(currentUser);
        Database.saveUsersData(userRepo.getUsersList(), Database.usersDatabase);
    }
}
