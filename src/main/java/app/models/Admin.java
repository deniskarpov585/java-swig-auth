package app.models;

import java.io.Serializable;

public class Admin extends BaseUser implements Serializable {
    private static final long serialVersionUID = 1L;
    private boolean passwordChanged = false;

    public Admin() {
        super();
    }

    public Admin(String login, String password) {
        super(login, password);
    }

    public boolean getPasswordChanged() { return passwordChanged; }
    public void setPasswordChanged(boolean change) { this.passwordChanged = change; }

}
