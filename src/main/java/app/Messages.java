package app;

public class Messages {

        public static String passwordMismatchError = "Passwords do not match. Try again!";

        public static String passwordEmptyError = "The password must not be empty!";
        public static String passwordChanged = "Password updated successfully";

        public static String listUsersUserExists = "User already exists";
        public static String noLogin = "Login not set. User not added";
        public static String listUsersUserAdded = "User added successfully";

        public static String emptyUserList = "Empty list of users";

        public static String attemptsLeft = "<br>Remaining attempts %d</html>";
        public static String userNotExists = "<html>The user does not exist." + attemptsLeft;
        public static String passwordFailed = "<html>Invalid password." + attemptsLeft;
        public static String userPasswordConstraints = "The password must contain upper and lower case letters!";
        public static String userBlocked = "<html>User blocked " + attemptsLeft;

        public static String adminChangeEmptyPassport = "You need to change the empty password!";

        public static String refuseToEnterPassPhrase = "<html>Refusal to enter a passphrase. " + "<br>" + "Program finished.</html>";
        public static String invalidPassphrase = "<html>Invalid passphrase. " + "<br>" + "Program finished.</html>";
        public static String mismatchPassphrase = "<html>Passphrases mismatched. " + "<br>" + "Program finished.</html>";

        public static String successInstallation = "<html>Success installation!</html>";
        public static String failInstallation = "<html>Failed installation!</html>";
        public static String signatureMismatch = "<html> Signature does not match </html>";

        public static String aboutMessage = "<html>" +
                "Author: Anton Golovin" + "<br>" +
                "group: A-13b-19" + "<br>" +
                "option: 2" + "<br>" +
                "encryption type: flow" + "<br>" +
                "encryption mode: -" + "<br>" +
                "random bumber: true" + "<br>" +
                "algorithm: md2" + "</html>";

}
