package app;

import app.models.Admin;
import app.models.User;
import net.miginfocom.swing.MigLayout;
import app.repo.UserRepo;
import javax.swing.*;
import java.awt.*;
import java.util.Objects;

public class AddUsersFrame extends JFrame {

    JTextField firstNameTextField = new JTextField(15);
    UserRepo userRepo;
    private JLabel messageLabel = new JLabel();

    AddUsersFrame(Admin admin, UserRepo userRepo) {
        JPanel mainPanel = new JPanel();
        mainPanel.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));

        this.setSize(350, 250);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setTitle("Adding Users");

        JPanel firstNamePanel = createFirstNamePanel();
        JPanel buttons = createButtons();

        mainPanel.add(firstNamePanel);
        mainPanel.add(buttons);

        this.userRepo = userRepo;
        this.add(mainPanel);
        this.setVisible(true);
    }

    JPanel createFirstNamePanel() {
        JPanel panel = new JPanel(new MigLayout("wrap 2, insets 5"));
        JLabel firstNameLabel = new JLabel("Username");

        messageLabel.setVisible(false);
        messageLabel.setText("");

        panel.add(firstNameLabel);
        panel.add(firstNameTextField);
        panel.add(messageLabel, "span 2");

        return panel;
    }

    JPanel createButtons() {
        JPanel panel = new JPanel(new MigLayout("wrap 2, insets 30"));

        JButton okButton = new JButton("Ok");
        okButton.addActionListener(e -> {
            String newUserLogin = firstNameTextField.getText();
            if (newUserLogin.equals("")) {
                messageLabel.setVisible(true);
                messageLabel.setForeground(new Color(206, 34, 34));
                messageLabel.setText(Messages.noLogin);
            } else {
                long collisions = userRepo.getUsersList()
                        .stream().filter(user -> Objects.equals(user.getLogin(), newUserLogin)).count();
                messageLabel.setVisible(true);
                if (collisions == 0) {
                    User user = new User(newUserLogin, "");
                    userRepo.addUser(user);
                    messageLabel.setForeground(new Color(162, 177, 60));
                    messageLabel.setText(Messages.listUsersUserAdded);
                } else {
                    messageLabel.setForeground(new Color(206, 34, 34));
                    messageLabel.setText(Messages.listUsersUserExists);
                }
            }
        });
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> {
            this.dispose();
        });

        panel.add(okButton);
        panel.add(cancelButton);

        return panel;
    }

}
