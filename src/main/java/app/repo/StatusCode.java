package app.repo;

public enum StatusCode {

    CODE_CORRECT(0),
    CODE_INCORRECT(-1),
    CODE_BLOCKED(-2),
    CODE_NOT_AUTH(-3);

    private int code;

    StatusCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
