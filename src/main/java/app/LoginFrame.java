package app;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;

import app.models.Admin;
import app.models.User;
import net.miginfocom.swing.MigLayout;
import app.repo.AdminRepo;
import app.repo.StatusCode;
import app.repo.UserRepo;
import app.utils.AttemptsController;
import app.utils.CryptoController;

import java.awt.*;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;

public class LoginFrame extends JFrame {

    private final UserRepo userRepo = new UserRepo();
    private final AdminRepo adminRepo = new AdminRepo();
    private final AttemptsController attemptsController = new AttemptsController(3);
    private CryptoController cryptoController;

    private final User user = new User();
    private final Admin admin = new Admin();

    JPasswordField passwordField = new JPasswordField(15);
    JTextField firstNameTextField = new JTextField(15);
    JLabel passwordFailedLabel = new JLabel();

    private static Logger LOGGER = Logger.getLogger(LoginFrame.class.getName());

    LoginFrame(CryptoController cryptoController) {
        JPanel loginPanel = new JPanel();
        loginPanel.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));

        this.setSize(450, 270);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Enter the program");

        this.cryptoController = cryptoController;
        JPanel loginFormPanel = createLoginFormPanel();
        JPanel loginButtonsPanel = createLoginButtonsPanel();

        this.setJMenuBar(createMenuBar());
        loginPanel.add(loginFormPanel);
        loginPanel.add(loginButtonsPanel);

        this.add(loginPanel);
        this.setVisible(true);
        this.setResizable(true);
    }

    JMenuBar createMenuBar() {
        JMenu menu = new JMenu("Help");
        JMenuBar mb = new JMenuBar();
        JMenuItem item = new JMenuItem("About");
        item.addActionListener(e ->
                JOptionPane.showMessageDialog(
                        menu,
                        Messages.aboutMessage,
                        "About",
                        JOptionPane.INFORMATION_MESSAGE
                ));
        menu.add(item);
        mb.add(menu);
        return mb;
    }

    JPanel createLoginFormPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new MigLayout("wrap 2, insets 5"));

        JLabel firstNameLabel = new JLabel("Username");
        JLabel passwordLabel = new JLabel("Password");

        firstNameTextField.setText("");
        passwordField.setEchoChar('*');
        passwordField.setText("");
        passwordFailedLabel.setVisible(false);
        passwordFailedLabel.setForeground(new Color(206, 34, 34));

        panel.add(firstNameLabel);
        panel.add(firstNameTextField);
        panel.add(passwordLabel);
        panel.add(passwordField);
        panel.add(passwordFailedLabel, "span 2");
        
        return panel;
    }

    JPanel createLoginButtonsPanel() {
        JPanel panel = new JPanel(new MigLayout("wrap 2, insets 30"));

        JButton okButton = new JButton("Ok");
        okButton.addActionListener(e -> {

            String login = firstNameTextField.getText();
            String password = new String(passwordField.getPassword());

            if (adminRepo.isAdmin(login)) {
                var adminAuth = adminRepo.auth(login, password);
                var isFirstEnter = adminAuth.getSecond();
                if (adminAuth.getFirst()) {
                    ControlFrame controlFrame = new ControlFrame(new Admin(login, password), true, isFirstEnter, userRepo);
                    handleAttempts(false);
                } else {
                    handleAttempts(true);
                }
            } else {
                if (userRepo.isUserExists(login)) {
                    var auth = userRepo.auth(login, password);
                    if (auth.getFirst()) {
                        ControlFrame adminFrame = new ControlFrame(new User(login, password), false, false, userRepo);
                        handleAttempts(false);
                    } else {
                        handleAttempts(true);
                    }
                    handleStatusCode(auth.getSecond());
                } else {
                    showUserNotExists();
                }
            }
        });
        JButton closeButton = new JButton("Close");
        closeButton.addActionListener(e -> {
            try {
                cryptoController.saveEncryptedFile();
            } catch (NoSuchPaddingException | IllegalBlockSizeException |
                    BadPaddingException | IOException |
                    InvalidKeyException | NoSuchAlgorithmException |
                    InvalidAlgorithmParameterException noSuchPaddingException) {
                noSuchPaddingException.printStackTrace();
            } finally {
                cryptoController.deleteTmpFiles();
                this.dispose();
            }
        });

        panel.add(okButton);
        panel.add(closeButton);

        return panel;
    }

    private void handleStatusCode(StatusCode sc) {
        switch (sc) {
            case CODE_BLOCKED:
                passwordFailedLabel.setVisible(true);
                passwordFailedLabel.setText(String.format(Messages.userBlocked, attemptsController.getAttempts()));
                break;

            case CODE_NOT_AUTH:
                passwordFailedLabel.setVisible(true);
                passwordFailedLabel.setText(String.format(Messages.passwordFailed, attemptsController.getAttempts()));
                break;

            case CODE_CORRECT:
                passwordFailedLabel.setText("");
                passwordFailedLabel.setVisible(false);
                break;
            default:
                break;
        }
    }
    private void showUserNotExists() {
        passwordFailedLabel.setVisible(true);
        passwordFailedLabel.setText(String.format(Messages.userNotExists, attemptsController.getAttempts()));
    }

    private void showAttempts() {
        passwordFailedLabel.setVisible(true);
        passwordFailedLabel.setText(String.format(Messages.passwordFailed, attemptsController.getAttempts()));
    }

    private void resetInfoLabel() {
        passwordFailedLabel.setVisible(false);
        passwordFailedLabel.setText("");
    }

    private void handleAttempts(boolean isFailed) {
        if (isFailed) {
            attemptsController.substituteAttempts();
            showAttempts();
        } else {
            attemptsController.resetAttempts();
            resetInfoLabel();
        }
        if (attemptsController.checkExcess()) this.dispose();
    }

}