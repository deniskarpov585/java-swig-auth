package app.repo;

import kotlin.Pair;
import lombok.Data;
import app.models.Admin;
import app.utils.PasswordManager;

import java.util.Objects;
import java.util.logging.Logger;

@Data
public class AdminRepo implements RepoInterface {

    public static String START_ADMIN_LOGIN = "admin";
    public static String START_ADMIN_PASSWORD = "";
    private static Logger LOGGER = Logger.getLogger(AdminRepo.class.getName());
    private PasswordManager passwordManager = new PasswordManager();

    private Admin admin;

    private boolean checkAdminInitialSettings(String login, String openPassword) {
        return  Objects.equals(admin.getPassword(), START_ADMIN_PASSWORD) &&
                Objects.equals(admin.getLogin(), START_ADMIN_LOGIN) &&
                Objects.equals(login, START_ADMIN_LOGIN) &&
                Objects.equals(openPassword, START_ADMIN_PASSWORD);
    }

    public boolean isAdmin(String login) {
        return Objects.equals(login, START_ADMIN_LOGIN);
    }

    public Pair<Boolean, Boolean> auth(String login, String openPassword) {
        admin = Database.readAdminData(Database.adminDatabase);
        String hashedPassword = passwordManager.getHash(openPassword);

        if (checkAdminInitialSettings(login, openPassword))
            return new Pair<>(true, true);
        else {
            if (checkLoginData(admin, login, hashedPassword))
                return new Pair<>(true, false);
            else {
                if (admin.getPasswordChanged())
                    LOGGER.info("Admin has changed password. Failed login attempt");
                else
                    LOGGER.info("Failed login attempt");
            }
        }
        return new Pair<>(false, false);
    }

}
