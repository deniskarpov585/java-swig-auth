package app.utils;

import lombok.Getter;
import lombok.Setter;

public class AttemptsController {

    @Setter @Getter private int attempts = 3;

    public AttemptsController(int attempts) {
       this.attempts = attempts;
    }

    public int getAttempts() { return this.attempts; }
    public void setAttempts(int attempts) { this.attempts = attempts; }

    public boolean substituteAttempts() {
        if (this.attempts > 0) {
            this.attempts --;
            return true;
        }
        else {
            setAttempts(3);
            return false;
        }
    }

    public boolean checkExcess() {
        return getAttempts() == 0;
    }

    public void resetAttempts() {
        setAttempts(3);
    }
}
