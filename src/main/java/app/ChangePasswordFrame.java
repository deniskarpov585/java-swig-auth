package app;

import app.models.Admin;
import app.models.BaseUser;
import app.models.User;
import net.miginfocom.swing.MigLayout;
import app.repo.Database;
import app.repo.UserRepo;
import app.utils.PasswordManager;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Logger;

public class ChangePasswordFrame extends JFrame {

    private static Logger LOGGER = Logger.getLogger(ChangePasswordFrame.class.getName());

    private String newOpenPassword = "";
    private JPasswordField newPasswordTextField = new JPasswordField(15);
    private JPasswordField confirmTextField = new JPasswordField(15);
    private JLabel messageLabel= new JLabel();
    private BaseUser user;
    private UserRepo userRepo;
    private PasswordManager passwordManager;

    ChangePasswordFrame(BaseUser user, UserRepo userRepo) {
        JPanel changePasswordPanel = new JPanel();
        changePasswordPanel.setBorder(BorderFactory.createEmptyBorder(15,15,15,15));

        this.setSize(420, 250);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setTitle("Change password");

        passwordManager = new PasswordManager();
        if (user instanceof User)
            this.user = userRepo.reloadUserData((User) user);
        else
            this.user = user;
        this.userRepo = userRepo;

        JPanel passwordPanel = createPasswordPanel();
        JPanel buttons = createButtonsPanel();

        changePasswordPanel.add(passwordPanel);
        changePasswordPanel.add(buttons);

        this.add(changePasswordPanel);
        this.setVisible(true);
        this.setResizable(true);
    }

    JPanel createPasswordPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new MigLayout("wrap 2, insets 5"));

        JLabel newPasswordLabel = new JLabel("New password");
        newPasswordTextField.setEchoChar('*');
        JLabel confirmLabel = new JLabel("Confirm");
        confirmTextField.setEchoChar('*');

        panel.add(newPasswordLabel);
        panel.add(newPasswordTextField);
        panel.add(confirmLabel);
        panel.add(confirmTextField);
        panel.add(messageLabel, "span 2");
        return panel;
    }

    JPanel createButtonsPanel() {
        JPanel panel = new JPanel(new MigLayout("wrap 2, insets 30"));
        JButton okButton = new JButton("Ok");

        messageLabel.setVisible(false);
        messageLabel.setText("");
        okButton.addActionListener(e -> {

            messageLabel.setVisible(true);
            if (!new String(newPasswordTextField.getPassword()).equals(new String(confirmTextField.getPassword()))) {
                messageLabel.setForeground(new Color(206, 34, 34));
                messageLabel.setText(Messages.passwordMismatchError);
            } else if (new String(newPasswordTextField.getPassword()).equals("") ||
                    new String(confirmTextField.getPassword()).equals("")) {
                messageLabel.setForeground(new Color(206, 34, 34));
                messageLabel.setText(Messages.passwordEmptyError);
            } else {
                if (user instanceof User) {
                    User u = (User) user;
                    if (u.isRestricted()) {
                        if (u.validatePassword(new String(newPasswordTextField.getPassword()))) {
                            LOGGER.info("The user entered the correct data\n");
                            messageLabel.setForeground(new Color(162, 177, 60));
                            changePassword();
                            messageLabel.setText(Messages.passwordChanged);
                        } else {
                            LOGGER.info("The user entered incorrect data\n");
                            messageLabel.setForeground(new Color(206, 34, 34));
                            messageLabel.setText(Messages.userPasswordConstraints);
                        }
                    } else {
                        successChanged();
                    }
                } else {
                    successChanged();
                }

            }
            newPasswordTextField.setText("");
            confirmTextField.setText("");
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> {
            this.dispose();
        });

        panel.add(okButton);
        panel.add(cancelButton);

        return panel;
    }

    private void successChanged() {
        messageLabel.setForeground(new Color(162, 177, 60));
        changePassword();
        messageLabel.setText(Messages.passwordChanged);
    }

    private void changePassword() {
        newOpenPassword = new String(newPasswordTextField.getPassword());
        String hashedNewPassword = passwordManager.getHash(newOpenPassword);
        user.setPassword(hashedNewPassword);

        if (user instanceof User) {
            userRepo.changeUserData((User) user);
            Database.saveUsersData(userRepo.getUsersList(), Database.usersDatabase);
        } else if (user instanceof Admin) {
            ((Admin) user).setPasswordChanged(true);
            Database.saveAdminData(user, Database.adminDatabase);
        }

    }
}
