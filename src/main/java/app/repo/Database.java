package app.repo;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import app.models.BaseUser;
import app.models.Admin;
import app.models.User;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Database {

    private static Logger LOGGER = Logger.getLogger(Database.class.getName());

    public static final String usersDatabase = "tmp.user.json";
    public static final String adminDatabase = "tmp.admin.json";

    public static void createDatabase(String dbname) {
        Admin admin = new Admin(AdminRepo.START_ADMIN_LOGIN, AdminRepo.START_ADMIN_PASSWORD);
        List<User> userList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writeValue(new File(adminDatabase), admin);
            objectMapper.writeValue(new File(usersDatabase), userList);
            LOGGER.info("Admin is created");
        } catch (IOException e) {
            LOGGER.warning("Error while creating  admin: " + e + "\n\n");
        }
    }

    public static boolean saveAdminData(BaseUser user, String dbname) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(new File(dbname), user);
            LOGGER.info(String.format("Admin %s is written", user.getLogin()));
        } catch (IOException e) {
            LOGGER.warning("Error while recording admin\n");
            return false;
        }
        return true;
    }

    public static boolean saveUsersData(List<User> user, String dbname) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(new File(dbname), user);
            LOGGER.info("Users recorded");
        } catch (IOException e) {
            LOGGER.warning("Error while recording users\n");
            return false;
        }
        return true;
    }

    public static Admin readAdminData(String dbname) {
        ObjectMapper objectMapper = new ObjectMapper();
        Admin adm = null;
        try {
            adm = objectMapper.readValue(new File(dbname), Admin.class);
        } catch (IOException | NullPointerException | ClassCastException e) {
            LOGGER.warning("Error while reading admin: " + e + "\n\n");
            return null;
        }
        return adm;
    }

    public static ArrayList<User> readUsers(String dbname) {
        ArrayList<User> usersList = new ArrayList<>();
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            usersList = objectMapper.readValue(new File(dbname), new TypeReference<ArrayList<User>>(){});
        } catch (Exception e) {
            LOGGER.warning(String.format("Error while reading users\n%s", e));
            return null;
        }
        return usersList;
    }

    public static void deleteUsersInfo() {
        if (new File(usersDatabase).delete())
            LOGGER.info("Users are deleted. The program is finished\n");
        else
            LOGGER.info("Data could not be deleted.");
    }

}
